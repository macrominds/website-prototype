#!/usr/bin/env bash
# requires imagemagick and webp

# exit when a command fails
set -o errexit
# return the exit status of the last command that threw a non-zero exit code
set -o pipefail
# exit when script tries to use undeclared variables
set -o nounset

# set first param to width, widthxheight or xheight, e.g. '1920', '1920x1920', 'x1920' to either fit the width, the box or the height
XL=('XL' '1632' 100 45)
L=('L' '960' 100 60)
M=('M' '1330' 100 60)
S=('S' '480' 100 60)
SETTINGS=(
	XL[@]
	L[@]
	M[@]
	S[@]
)

CONVERT_TARGET_PATH="public/images/"

function convert_file() {
	{ #try
	    FULL_FILE_NAME="${1}"
	    ALIAS="${2}"
		GEOMETRY="${3}"
		SHARPNESS="${4}"
		QUALITY="${5}"
		FILE_NAME="${FULL_FILE_NAME##*/}"
		FILE_PATH="${CONVERT_TARGET_PATH}${ALIAS}/"

		ICC_NAME="${FILE_PATH}IMsource.icc"

		# RGB OR sRGB (sRGB produces deeper and darker colors as it seems)
		WORKING_COLORSPACE=RGB

		[[ -d $FILE_PATH ]] || mkdir -p $FILE_PATH
		
		# TODO Correctly extract color profile
		#
		# See http://www.imagemagick.org/Usage/formats/#profiles
		# identify -verbose ${FULL_FILE_NAME} | grep 'Profile-.*bytes'
		# convert convert -define jpeg:size=64x64  image.jpg  iptc:"${FILE_PATH}${ICC_NAME}"

		# Lossless scale
		convert \( \
					"${FULL_FILE_NAME}" \
					-type TrueColor \
					-depth 16 \
					-set colorspace sRGB \
					-colorspace ${WORKING_COLORSPACE} \
					-define filter:c=0.1601886205085204 \
					-filter Cubic \
					-distort Resize "$GEOMETRY" \
					-auto-orient \
				\) \
				\( \
					-clone 0 \
					-gamma 3 \
					-define convolve:scale=${SHARPNESS}%,100 \
					-morphology Convolve DoG:3,0,0.4981063336734057 \
					-gamma 0.3333333333333333333 \
				\) \
				\( \
					-clone 0 \
					-define convolve:scale=${SHARPNESS}%,100 \
					-morphology Convolve DoG:3,0,0.4806768770037563 \
				\) \
				-delete 0 \
				\( \
					-clone 1 \
					-colorspace gray \
					-auto-level \
				\) \
				-compose over \
				-composite \
				-set colorspace ${WORKING_COLORSPACE} \
				-colorspace sRGB \
				-quality 100% \
				-sampling-factor 4:4:4 \
				-define png:preserve-iCCP \
				-auto-orient \
				-compress Lossless "${FILE_PATH}${FILE_NAME}.uncompressed"
		# Compress jpg
		convert -interlace None \
			-quality ${QUALITY}% \
			"${FILE_PATH}${FILE_NAME}.uncompressed" \
			"${FILE_PATH}${FILE_NAME}"

		# Create webp
		cwebp "${FILE_PATH}${FILE_NAME}.uncompressed" -progress -o "${FILE_PATH}${FILE_NAME%.*}.webp"

		# Cleanup
		rm "${FILE_PATH}${FILE_NAME}.uncompressed"

	} || { #catch
		echo "An error occured during conversion to ${FILE_PATH}${FILE_NAME}. This might not be fatal."
	}
}

for FILE in "$@"
do
	COUNT=${#SETTINGS[@]}
        for ((i=0; i<$COUNT; i++))
	do
	    ALIAS=${!SETTINGS[i]:0:1}
		GEOMETRY=${!SETTINGS[i]:1:1}
		SHARPNESS=${!SETTINGS[i]:2:1}
		QUALITY=${!SETTINGS[i]:3:1}

		convert_file "${FILE}" $ALIAS $GEOMETRY $SHARPNESS $QUALITY
	done

done
