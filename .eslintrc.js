module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'plugin:vue/essential',
    'plugin:ava/recommended',
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'module-resolver',
    'vue',
    'ava'
  ],
  rules: {
  },
  settings: {
    'import/resolver': {
      'babel-module': {}
    }
  }
};
