export default {
  files: ['tests/js/unit/**/*'],
  sources: ['resources/js/**/*'],
  failFast: true,
  require: [
    './tests/js/_ava-babel-register.js'
  ]
}