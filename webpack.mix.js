/**
* This file is used to configure laravel-mix.
* Laravel-mix is used to simplify webpack, that is actually working behind the scenes.
* See https://laravel-mix.com/
*/

const mix = require('laravel-mix');
require('laravel-mix-purgecss');

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
		.copy('resources/index.html', 'public/index.html')
    .purgeCss({
        // enabled: true, // <- is enabled by default in prod mode. Use this to debug in dev mode
        //
        // If you encounter css that is removed but needed, then whitelist it according to the docs:
        // https://www.purgecss.com/whitelisting
        //
        // whitelist: []
    })
    .options({
    	postCss: [
    		// autoprefixer is included by laravel-mix out-of-the-box
    		// 
    		// postcss-normalize replaces `@import "normalize.css"` with a reduced and unopinionated version
    		// that is required for the set of browsers, you defined in your package.json browserslist.
    		// See https://github.com/csstools/postcss-normalize
    		require('postcss-normalize'),
    	]
    });
