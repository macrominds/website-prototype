// ----
// Example class for unit- and integration-testing
// ----

export default class LinkTransformer {
  constructor(query) {
    this.query = query;
  }

  getQuery() {
    return this.query;
  }

  find() {
    return document.querySelectorAll(this.query);
  }

  transform(nodeList = null) {
    const nodes = nodeList || this.find();
    nodes.forEach((node) => {
      node.setAttribute('target', '_blank');
    });
  }
}
