// ----
// Example for using mocks and spies. Taken from sinonjs.org
// ----

function once(fn) {
  let returnValue; let
    called = false;
  return function o(...args) {
    if (!called) {
      called = true;
      returnValue = fn.apply(this, args);
    }
    return returnValue;
  };
}

export { once as default };
