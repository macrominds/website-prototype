require('dotenv').config();
const config = require('./tests/js/vrt/mainConfig.js');

const projectId = process.env.APP_ID;
const baseUrl = process.env.APP_URL;

const relativeUrls =[
  "/",
  "/kontakt.html",
];

// TODO find a way to make form fields filled in

const viewportConfig = {
  phone: {
    width: 320,
    height: 480,
  },
  tablet: {
    width: 1024,
    height: 768,
  },
  laptop: {
    width: 1330,
    height: 768,
  },
  desktop: {
    width: 1920,
    height: 1200,
  }
}

module.exports = config({
  baseUrl,
  projectId,
  relativeUrls,
  viewportConfig,
});


