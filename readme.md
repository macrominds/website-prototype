# Prototype

This is a basic skeleton for a website project.

In includes scss, es6 and asset processing (like image optimization), audits for dependencies, 
vulnerabilities, browser compatibility and (later) performance. It is prepared for unit testing, 
integration testing, e2e testing and visual regression testing.

## Requirements

As of 2019-08-08:

* yarn 1.17.3
* node 10.16.2
* npm 6.9.0

## Setup

### Locally 

After cloning the project, copy [.env.example](.env.example) to `.env`, adapt it to your system and run:

```bash
# installs the node dependencies
$ yarn
```

### Server

Let your server point to [public](public) as the docroot.

##### Python example

```bash
$ cd public
$ python3 -m http.server 8080
```

##### Php example

```bash
$ php -S localhost:8080 -t ./public/
```

## Maintenance

### Audits

Run `yarn audit:all` to run almost all audits at once.
Run `yarn audit:all-but-deps` to exclude dependency audits. (The dependency audit may sometimes report errors 
caused by transient dependencies that cannot be fixed).

`yarn audit:all` excludes `yarn audit:performance` as that is a time consuming audit that is performed 
best on prod builds, while other audits are best performed on dev builds.

#### Dependencies / Vulnerabilities / Up-To-Dateness

Regularly run the following audits:

```
# check if the package.json matches the yarn.lock
# check for outdated packages
# check for known vulnerabilities
yarn audit:dependencies
```

#### Supported Browsers

```
# List supported browsers
yarn audit:browsers
```

#### Check for unsupported css browser features

This checks your css (not your scss) files for unsupported browser features based on [browserslist](./package.json).

```
yarn audit:styles
```

Notice: If you want to ignore some parts, use 

```
/* stylelint-disable */
…
/* stylelint-enable */
```

See [turning rules off from within your css](https://stylelint.io/user-guide/configuration#turning-rules-off-from-within-your-css). 

However, these comments will not be preserved on a production build (`yarn prod`). So, just for your interest: you will encounter lots of messages on a production build.

#### Check for javascript coding style

```
yarn audit:js
```

In order to fix automatically fixable issues, run

```
yarn audit:js --fix
``` 

## Audit Website Performance

Run `yarn audit:performance` on a prod build to perform the following audits:

* size (absolute size in configured budget)
* heap snapshot (browser load in configured budget)
* unused source size (percentage of unused sources in configured budget)
* google lighthouse checks (performance, accessibility, best practices, seo, pwa in configured budget)

Configure the budget with [.gimbalrc.yml](./.gimbalrc.yml). 

### Testing

Run all unit and integration tests (and rerun them when code changes):

```
yarn test --watch
```

Skip the `--watch` flag to run the tests only once. In order to run only a specific type of tests, run:

```
yarn test:unit
yarn test:integration
``` 

Run e2e Tests with nightwatch (and chromedriver):

```bash
# run a server. E.g. 
php -S 192.168.2.103:8080 -t ./public/
# then run the test
yarn test:e2e
```

Run all unit, integration, e2e and visual regression tests:

```
yarn test:all
```

Screenshots will automatically be generated upon e2e test failure. 
Find your e2e screenshots and reports in [reports/e2e](reports/e2e).

#### Visual regression testing

We're using [BackstopJS](https://github.com/garris/BackstopJS) with `--docker`, so you need to have docker installed
on your system. 

To run visual regression tests, make sure to adapt your `.env` file to have the proper `APP_URL` and make sure that
it is served properly. Then run:

```
# run a server. E.g. 
php -S 192.168.2.103:8080 -t ./public/
# then run the test
yarn test:vrt:check
```

To approve changes:

```
yarn test:vrt:approve
```

Make sure to keep the list of pages and other config up to date in [vrt.config.js](vrt.config.js).

## Understanding the Concepts

### Folderstructure

[public](public) is the docroot. Most files inside this directory are generated, except for [index.php](public/index.php), [.htaccess](public/.htaccess), etc. 

Laravel-mix (effectively webpack) processes assets, [stylesheets](resources/sass) and [scripts](resources/js) from [resources](resources).

### Short es6 import paths

We're using 
[babel-plugin-module-resolver](https://github.com/tleunen/babel-plugin-module-resolver), 
[eslint-import-resolver-babel-module](https://github.com/tleunen/eslint-import-resolver-babel-module) and 
[eslint-plugin-module-resolver](https://github.com/HeroProtagonist/eslint-plugin-module-resolver) to
be able to use import paths relative to our js root instead of clunky `../../../../resources/js/` paths.

Adapt [.babelrc](.babelrc) to add or change paths.
 
### Browsersupport

Configure the browsersupport in [.browserslistrc](.browserslistrc). 

It will be used to autoprefix your css and to audit the resulting css against unsupported browser-features.

See for more information:

* [browserslist](https://www.npmjs.com/package/browserslist)
* [stylelint](https://github.com/stylelint/stylelint)
* [stylelint-config-recommended](https://github.com/stylelint/stylelint-config-recommended)
* [stylelint-no-unsupported-browser-features](https://github.com/ismay/stylelint-no-unsupported-browser-features)
* [doiuse](https://github.com/anandthakker/doiuse)

It will also be used to select only the normalizations required for your supported browser-set, by using [postcss-normalize](https://github.com/csstools/postcss-normalize). 

Unfortunately, we could not configure this in [package.json](package.json), because laravel-mix@4.1.2 would neither load the package.json config nor a postcss.config.js, although [it seems to be prepared for that](https://github.com/JeffreyWay/laravel-mix/blob/fc3bc9f57ac1298b40e72d789d1c344e9b5cf668/src/components/Css.js#L117).

### Gotyas

Notice that you cannot simply move the babel config from [.babelrc](.babelrc) to [package.json](package.json). 
Babel will stop adding the required polyfills when transpiling. 
This only works with [.babelrc](.babelrc) as it seems. This is very likely caused by laravel-mix only looking for
the exact filepath [.babelrc](.babelrc) to load the config.

Also: When you tinker around with the config of .babelrc or .browserslistrc, make sure to change your app.js source.
Otherwise, a cache will be used and your changes are not applied sometimes. You will see if babel actually
transpiled anything by scrolling up after running `yarn dev`. There should be debug output that tells
you which browsers and polyfills have been addressed.

Debug output is turned on in .babelrc (and turned off there when testing (`"env": { "test": …`)).

## Known issues

### node dependency issues:

At the time of this writing (2019-09-30), the following node dependency problems are known:

#### Not eslint 6 compatible
 
* [eslint-plugin-vue](https://github.com/vuejs/eslint-plugin-vue/pull/917) (most likely because of vue-eslint-parser)
* [vue-eslint-parser](https://github.com/mysticatea/vue-eslint-parser/pull/51) 
