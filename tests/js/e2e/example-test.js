require('dotenv').config();
// ----
// Example from https://github.com/nightwatchjs/nightwatch
// ----

module.exports = {
  'Demo test Google': function demoTestGoogle(client) {
    client
      .url(process.env.APP_URL + '/index.html')
      .waitForElementVisible('body', 1000)
      .assert.title('Example Document')
      .assert.visible('input[type=text]')
      .setValue('input[type=text]', 'rembrandt van rijn')
      .end();
  },
};
