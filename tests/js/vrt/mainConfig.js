module.exports = function o({
  baseUrl, projectId, relativeUrls, viewportConfig,
}) {
  const viewports = [];
  Object.keys(viewportConfig).forEach((k) => {
    viewports.push({
      label: k,
      width: viewportConfig[k].width,
      height: viewportConfig[k].height,
    });
  });

  const basicConfig = {
    baseUrl,
    projectId,
    relativeUrls,
    viewports,
  };
  const scenarios = [];

  basicConfig.relativeUrls.forEach((relativeUrl) => {
    scenarios.push({
      label: relativeUrl,
      url: `${basicConfig.baseUrl}${relativeUrl}`,
      delay: 0,
      selectorExpansion: true,
      expect: 0,
      misMatchThreshold: 0 /* 0.1 */,
      requireSameDimensions: true,
    });
  });

  return {
    id: basicConfig.projectId,
    viewports,
    onBeforeScript: 'puppet/onBefore.js',
    onReadyScript: 'puppet/onReady.js',
    scenarios,
    paths: {
      bitmaps_reference: 'reports/vrt/bitmaps_reference',
      bitmaps_test: 'reports/vrt/bitmaps_test',
      engine_scripts: 'tests/js/vrt/engine_scripts',
      html_report: 'reports/vrt/html_report',
      ci_report: 'reports/vrt/ci_report',
    },
    report: ['browser'],
    engine: 'puppeteer',
    engineOptions: {
      args: ['--no-sandbox'],
    },
    asyncCaptureLimit: 5,
    asyncCompareLimit: 50,
    debug: false,
    debugWindow: false,
    dockerCommandTemplate: 'docker run --rm -it --user $(id -u):$(id -g) --mount type=bind,source="{cwd}",target=/src backstopjs/backstopjs:{version} {backstopCommand} {args}',
  };
};
