// ----
// Used to transpile the code under test while ignoring the test cases and node_modules
// see ava.config.js and ava.integration.config.js
// ----

require('@babel/register')({
  ignore: ['node_modules/*', 'tests/js/integration/*', 'tests/js/unit/*'],
});
