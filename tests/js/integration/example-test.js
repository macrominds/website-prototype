// ----
// Example for integration testing
// ----

import test from 'ava/index';
import sinon from 'sinon';

import LinkTransformer from 'LinkTransformer';
import once from 'helpers';

test.beforeEach(() => {
  document.body.innerHTML = '<a href="#" rel="external">'
    + 'external</a><br>'
    + '<p>test</p>'
    + '<a href="#">internal</a>';
});

test('it finds elements for simple queries', (t) => {
  const linkTransformer = new LinkTransformer('a');
  const nodeList = linkTransformer.find();

  t.is(nodeList.length, 2);
});

test('it finds elements for complex queries', (t) => {
  const linkTransformer = new LinkTransformer('a[rel="external"]');
  const nodeList = linkTransformer.find();

  t.is(nodeList.length, 1);
});

test.serial('it transforms elements for simple queries', (t) => {
  const linkTransformer = new LinkTransformer('a');
  const nodeList = linkTransformer.find();
  linkTransformer.transform(nodeList);
  t.is(document.querySelectorAll('a[target="_blank"]').length, 2);
});

test.serial('it transforms elements for complex queries', (t) => {
  const linkTransformer = new LinkTransformer('a[rel="external"]');
  linkTransformer.transform();
  t.is(document.querySelectorAll('a[target="_blank"]').length, 1);
});

test('it calls the original function', (t) => {
  const callback = sinon.fake();
  const proxy = once(callback);

  proxy();

  t.true(callback.called);
});

test('it calls the original function only once', (t) => {
  const callback = sinon.fake();
  const proxy = once(callback);

  proxy();
  proxy();

  t.true(callback.calledOnce);
  // ...or:
  // assert.equals(callback.callCount, 1);
});

test('it calls original function with right this and args', (t) => {
  const callback = sinon.fake();
  const proxy = once(callback);
  const obj = {};

  proxy.call(obj, 1, 2, 3);

  t.true(callback.calledOn(obj));
  t.true(callback.calledWith(1, 2, 3));
});

// See sinonjs.org for more examples and a simple server fake

// ava can take snapshots. You should verify snapshots once you consume external services.
