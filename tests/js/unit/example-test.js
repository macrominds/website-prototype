// ----
// Example for unit testing
// ----

import test from 'ava/index';
import LinkTransformer from 'LinkTransformer';

test('it stores the getQuery', (t) => {
  const query = 'a';
  const transformer = new LinkTransformer(query);
  t.is(transformer.getQuery(), query);
});
