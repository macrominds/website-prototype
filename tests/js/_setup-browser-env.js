// ----
// Used to publish jsdom to globals like window, document, etc, in order to be able to test dom
// changes. Is activated in integration tests. See ava.integration.config.js
// ----

import browserEnv from 'browser-env';

browserEnv();
