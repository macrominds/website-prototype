import unitConfig from './ava.config.js';

export default {
  ...unitConfig,
  files: [
    'tests/js/integration/**/*',
  ],
  require: [
    ...unitConfig.require,
    './tests/js/_setup-browser-env.js'
  ]
}